.. DataMart API documentation master file, created by
   sphinx-quickstart on Wed Apr 17 12:31:26 2019.
   You can adapt this file completely to your liking, but it should at least
   contain the root `toctree` directive.

DataMart API's documentation
****************************

The DataMart systems conform to common APIs (REST and Python) that allow to query them and download result datasets.

The DataMart systems can be found here:

* `NYU's system <https://gitlab.com/ViDA-NYU/datamart/datamart>`__
* `ISI's system <https://github.com/usc-isi-i2/datamart>`__

Contents
========

.. toctree::
   :maxdepth: 2

   query_api
   rest_api
   python

Indices and tables
==================

* :ref:`genindex`
* :ref:`modindex`
* :ref:`search`
